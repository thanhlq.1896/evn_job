package com.hitex.evn_job;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class EvnJobElectricityApplication {

    public static void main(String[] args) {
        SpringApplication.run(EvnJobElectricityApplication.class, args);
    }

}
