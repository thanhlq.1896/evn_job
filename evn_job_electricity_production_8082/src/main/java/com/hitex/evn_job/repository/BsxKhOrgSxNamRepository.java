package com.hitex.evn_job.repository;

import com.hitex.evn_job.model.BsxKhOrgSxNam;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface BsxKhOrgSxNamRepository extends JpaRepository<BsxKhOrgSxNam, Integer> {
    @Query("SELECT b FROM BsxKhOrgSxNam b WHERE NOT EXISTS " +
            "(select 1 from BsxKhOrgSxNam old WHERE b.id = old.id)")
    List<BsxKhOrgSxNam> getListBsxKhOrgSxNam();
}
