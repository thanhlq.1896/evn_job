package com.hitex.evn_job.repository;

import com.hitex.evn_job.model.BsxVhPlantDay;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface BsxVhPlantDayRepository extends JpaRepository<BsxVhPlantDay, Integer> {
    @Query("SELECT b.id, b.plantId, b.day, b.utk FROM BsxVhPlantDay b WHERE NOT EXISTS " +
            "(select 1 from BsxVhPlantDay old WHERE b.id = old.id)")
    List<BsxVhPlantDay> getListBsxVhPlantDay();
}
