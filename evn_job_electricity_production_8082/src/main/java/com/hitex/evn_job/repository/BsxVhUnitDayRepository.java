package com.hitex.evn_job.repository;

import com.hitex.evn_job.model.BsxVhUnitDay;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface BsxVhUnitDayRepository extends JpaRepository<BsxVhUnitDay, Integer> {
    @Query("SELECT b.id, b.unitId, b.day, b.emf FROM BsxVhUnitDay b WHERE NOT EXISTS " +
            "(select 1 from BsxVhUnitDay old WHERE b.id = old.id)")
    List<BsxVhUnitDay> getListBsxVhUnitDay();
}
