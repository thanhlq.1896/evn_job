package com.hitex.evn_job.repository;

import com.hitex.evn_job.model.BsxKhPlantSxNam;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface BsxKhPlantSxNamRepository extends JpaRepository<BsxKhPlantSxNam, Integer> {
    @Query("SELECT b FROM BsxKhPlantSxNam b WHERE NOT EXISTS " +
            "(select 1 from BsxKhPlantSxNam old WHERE b.id = old.id)")
    List<BsxKhPlantSxNam> getListBsxKhPlantSxNam();
}
