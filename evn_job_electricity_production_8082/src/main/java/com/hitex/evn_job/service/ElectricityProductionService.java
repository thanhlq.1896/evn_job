package com.hitex.evn_job.service;


import com.hitex.evn_job.model.BsxKhOrgSxNam;
import com.hitex.evn_job.model.BsxKhPlantSxNam;
import com.hitex.evn_job.model.BsxVhPlantDay;
import com.hitex.evn_job.model.BsxVhUnitDay;

import java.util.List;

public interface ElectricityProductionService {
    List<BsxKhOrgSxNam> getListBsxKhOrgSxNamForJob();

    List<BsxKhPlantSxNam> getListBsxKhPlantSxNamForJob();

    List<BsxVhPlantDay> getListBsxVhPlantDayForJob();

    List<BsxVhUnitDay> getListBsxVhUnitDayForJob();

}
