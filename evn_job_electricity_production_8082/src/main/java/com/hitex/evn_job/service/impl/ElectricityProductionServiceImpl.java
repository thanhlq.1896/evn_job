package com.hitex.evn_job.service.impl;

import com.hitex.evn_job.model.BsxKhOrgSxNam;
import com.hitex.evn_job.model.BsxKhPlantSxNam;
import com.hitex.evn_job.model.BsxVhPlantDay;
import com.hitex.evn_job.model.BsxVhUnitDay;
import com.hitex.evn_job.repository.BsxKhOrgSxNamRepository;
import com.hitex.evn_job.repository.BsxKhPlantSxNamRepository;
import com.hitex.evn_job.repository.BsxVhPlantDayRepository;
import com.hitex.evn_job.repository.BsxVhUnitDayRepository;
import com.hitex.evn_job.service.ElectricityProductionService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Log4j2
public class ElectricityProductionServiceImpl implements ElectricityProductionService {

    @Autowired
    BsxKhOrgSxNamRepository bsxKhOrgSxNamRepository;

    @Autowired
    BsxKhPlantSxNamRepository bsxKhPlantSxNamRepository;

    @Autowired
    BsxVhPlantDayRepository bsxVhPlantDayRepository;

    @Autowired
    BsxVhUnitDayRepository bsxVhUnitDayRepository;

    @Override
    public List<BsxKhOrgSxNam> getListBsxKhOrgSxNamForJob() {
        return bsxKhOrgSxNamRepository.getListBsxKhOrgSxNam();
    }

    @Override
    public List<BsxKhPlantSxNam> getListBsxKhPlantSxNamForJob() {
        return bsxKhPlantSxNamRepository.getListBsxKhPlantSxNam();
    }

    @Override
    public List<BsxVhPlantDay> getListBsxVhPlantDayForJob() {
        return bsxVhPlantDayRepository.getListBsxVhPlantDay();
    }

    @Override
    public List<BsxVhUnitDay> getListBsxVhUnitDayForJob() {
        return bsxVhUnitDayRepository.getListBsxVhUnitDay();
    }
}
