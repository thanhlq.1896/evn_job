package com.hitex.evn_job.utils;

import java.time.format.DateTimeFormatter;

public class Constant {
    public static final String BSX_KH_PLANT_SX_NAME = "bsx_kh_plant_sx_nam";
    public static final String BSX_KH_ORG_SX_NAM = "bsx_kh_org_sx_nam";
    public static final String BSX_VH_PLANT_DAY = "bsx_vh_plant_day";
    public static final String BSX_VH_UNIT_DAY = "bsx_vh_unit_day";
    // Config mail
    public static final String GENERAL = "general";

}
