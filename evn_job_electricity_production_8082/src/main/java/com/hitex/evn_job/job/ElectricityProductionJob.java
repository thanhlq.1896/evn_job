package com.hitex.evn_job.job;

import com.hitex.evn_job.model.BsxKhOrgSxNam;
import com.hitex.evn_job.model.BsxKhPlantSxNam;
import com.hitex.evn_job.model.BsxVhPlantDay;
import com.hitex.evn_job.model.BsxVhUnitDay;
import com.hitex.evn_job.service.ElectricityProductionService;
import lombok.extern.log4j.Log4j2;
import okhttp3.*;
import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import java.io.IOException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@Component
@Log4j2
public class ElectricityProductionJob {

    long startTime = Instant.now().toEpochMilli();
    @Autowired
    private ElectricityProductionService electricityProductionService;

    public static void main(String[] args) {

        ArrayList<BsxKhOrgSxNam> listTest = new ArrayList<>();
        BsxKhOrgSxNam test = new BsxKhOrgSxNam();
        test.setOrgId("test Job 1");
        listTest.add(0, test);
        JSONArray arr = new JSONArray(listTest);
        String strRequest = "" +
                "{\r\n    " +
                "\"sessionId\": \"123456\",\r\n    " +
                "\"token\": \"123456\",\r\n    \"wsCode\": \"addBsxKhOrgSxNam\",\r\n    " +
                "\"wsRequest\": " +
                "{\r\n        " +
                "           \"orgId\":\"Job\",\r\n        " +
                "\"list\": " + arr +
                "}\r\n" +
                "}";
        log.info("Request : " + strRequest);
    }

    /**
     * Run every 24h
     */
    @Scheduled(cron = "${job.electricityProduction}")
    public void sendApiBsxKhOrgSxNam() throws Exception {
        List<BsxKhOrgSxNam> listBsxKhOrgSxNamForJob = electricityProductionService.getListBsxKhOrgSxNamForJob();
        if (ObjectUtils.isEmpty(listBsxKhOrgSxNamForJob)) {
            JSONArray arr = new JSONArray(listBsxKhOrgSxNamForJob);
            getApi(arr, "addBsxKhOrgSxNam");
        }

    }

    @Scheduled(cron = "${job.electricityProduction}")
    public void sendApiBsxVhUnitDay() throws Exception {
        List<BsxVhUnitDay> listBsxVhUnitDayForJob = electricityProductionService.getListBsxVhUnitDayForJob();
        if (ObjectUtils.isEmpty(listBsxVhUnitDayForJob)) {
            JSONArray arr = new JSONArray(listBsxVhUnitDayForJob);
            getApi(arr, "addBsxVhUnitDay");
        }

    }

    @Scheduled(cron = "${job.plant-electricityProduction}")
    public void sendApiBsxKhPlantSxNam() throws Exception {
        List<BsxKhPlantSxNam> listBsxKhPlantSxNamForJob = electricityProductionService.getListBsxKhPlantSxNamForJob();
        if (ObjectUtils.isEmpty(listBsxKhPlantSxNamForJob)) {
            JSONArray arr = new JSONArray(listBsxKhPlantSxNamForJob);
            getApi(arr, "addBsxKhPlantSxNam");
        }

    }

    @Scheduled(cron = "${job.plant-electricityProduction}")
    public void sendApiBsxVhPlantDay() throws Exception {
        List<BsxVhPlantDay> result = new LinkedList<>();
        List<BsxVhPlantDay> listBsxVhPlantDayForJob = electricityProductionService.getListBsxVhPlantDayForJob();
        if (ObjectUtils.isEmpty(listBsxVhPlantDayForJob)) {
            JSONArray arr = new JSONArray(listBsxVhPlantDayForJob);
            getApi(arr, "addBsxVhPlantDay");
        }

    }

    private void getApi(JSONArray arr, String nameApi) throws IOException {
        OkHttpClient client = new OkHttpClient().newBuilder()
                                                .build();
        MediaType mediaType = MediaType.parse("application/json");
        String strRequest = "" +
                "{\r\n    " +
                "\"sessionId\": \"123456\",\r\n    " +
                "\"token\": \"123456\",\r\n    \"wsCode\": \"" + nameApi + "\",\r\n    " +
                "\"wsRequest\": " +
                "{\r\n        " +
                "\"list\": " + arr +
                "}\r\n" +
                "}";
        log.info("Request : " + strRequest);
        RequestBody body = RequestBody.create(mediaType, strRequest);
        Request request = new Request.Builder()
                .url("http://localhost:8080/api/" + nameApi)
                .method("POST", body)
                .addHeader("Content-Type", "application/json")
                .build();
        Response response = client.newCall(request).execute();
        if (!response.isSuccessful()) {
            throw new IOException("Unexpected code " + response);
        }
        log.info("Time Taken (milliseconds): " + (Instant.now().toEpochMilli() - startTime));
        log.info("Date: " + response.header("Date"));
        log.info("Log API: " + response.body().string());
    }


}
