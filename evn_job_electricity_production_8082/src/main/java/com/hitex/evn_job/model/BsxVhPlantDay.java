package com.hitex.evn_job.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.hitex.evn_job.utils.Constant;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
@Table(name =  Constant.BSX_VH_PLANT_DAY,schema = Constant.GENERAL)
public class BsxVhPlantDay {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "plantid")
    private String plantId;
    @Column(name = "day")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime day;
    @Column(name = "UTK")
    private Double utk;
    @Column(name = "EMF")
    private Double emf;
    @Column(name = "EMF_K")
    private Double emfK;
    @Column(name = "EMF_DO")
    private Double emfDo;
    @Column(name = "EMF_FO")
    private Double emfFo;
    @Column(name = "EMF_DH")
    private Double emfDh;
    @Column(name = "ETD")
    private Double etd;
    @Column(name = "ETD_K")
    private Double etdK;
    @Column(name = "ETD_DO")
    private Double etdDo;
    @Column(name = "ETD_FO")
    private Double etdFo;
    @Column(name = "ETD_DH")
    private Double etdDh;
    @Column(name = "ETDSXD")
    private Double etdSxd;
    @Column(name = "ETDXS")
    private Double etdXs;
    @Column(name = "ETDCB")
    private Double etdCb;
    @Column(name = "EDB")
    private Double edb;
    @Column(name = "EDB_K")
    private Double edbK;
    @Column(name = "EDB_DO")
    private Double edbDo;
    @Column(name = "EDB_FO")
    private Double edbFo;
    @Column(name = "EDB_DH")
    private Double edbDh;
    @Column(name = "NLthantttc_SX")
    private Double nLThanTTTCSx;
    @Column(name = "NLthantttc_KD")
    private Double nLThanTTTCKd;
    @Column(name = "NLthantttc_SCL")
    private Double nLThanTTTCScl;
    @Column(name = "NLthantttc_T")
    private Double nLThanTTTCT;
    @Column(name = "NLthantttn_SX")
    private Double nLThanTTTNSx;
    @Column(name = "NLthantttn_KD")
    private Double nLThanTTTNKd;
    @Column(name = "NLthantttn_SCL")
    private Double nLThanTTTNScl;
    @Column(name = "NLthantttn_T")
    private Double nLThanTTTNT;
    @Column(name = "NLThantontc_T")
    private Double nLThanTonTCT;
    @Column(name = "NLthantontn_T")
    private Double nLThanTonTNT;
    @Column(name = "NLdaudott_SX")
    private Double nLDauDOTTSx;
    @Column(name = "NLdaudott_KD")
    private Double nLDauDOTTKd;
    @Column(name = "NLdaudott_SCL")
    private Double nLDauDOTTScl;
    @Column(name = "NLdaudott_T")
    private Double nLDauDOTTT;
    @Column(name = "NLdaudoton_T")
    private Double nLDauDOTon_T;
    @Column(name = "NLdaufott_SX")
    private Double nLDauFOTTSx;
    @Column(name = "NLdaufott_KD")
    private Double nLDauFOTTKd;
    @Column(name = "NLdaufott_SCL")
    private Double nLDauFOTTScl;
    @Column(name = "NLdaufott_T")
    private Double nLDauFOTTT;
    @Column(name = "NLdaufoton_T")
    private Double nLDauFOTonT;
    @Column(name = "NLkhitt_SX")
    private Double nLKhiTTSx;
    @Column(name = "NLkhitt_KD")
    private Double nLKhiTTKd;
    @Column(name = "NLkhitt_SCL")
    private Double nLKhiTTScl;
    @Column(name = "NLkhitt_T")
    private Double nLKhiTTT;
    @Column(name = "USER_ID_DIEN")
    private String userIdDien;
    @Column(name = "USER_DTIME_DIEN")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime userDtimeDien;
    @Column(name = "USER_ID_NL")
    private String userIdNl;
    @Column(name = "USER_DTIME_NL")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime userDtimeNl;
    @Column(name = "DIEN_NANG_NHAN")
    private Double dienNangNhan;
    @Column(name = "DIEN_NANG_NHAN_CHAY_BU")
    private Double dienNangNhanChayBu;
    @Column(name = "DIEN_NANG_PHAN_KHANG")
    private Double dienNangPhanKhang;
    @Column(name = "DO_PHAT_THAI")
    private Double doPhatThai;
    @Column(name = "TON_THAT_MBA_KICH_TU")
    private Double tonThatMbaKichTu;
    @Column(name = "TON_THAT_MBA_NANG")
    private Double tonThatMbaNang;
    @Column(name = "DAT_KHI_PHAT_THAI")
    private Double datKhiPhatThai;
    @Column(name = "AUTO_CAL_FORMULA")
    private Double autoCalFormula;
    @Column(name = "NLdavoiTT")
    private Double nLDaVoiTT;
    @Column(name = "NLdavoiton")
    private Double nLDaVoiTon;
    @Column(name = "NLdavoiTT_KD")
    private Double nLDaVoiTTKd;
    @Column(name = "NLdavoiTT_SCL")
    private Double nLDaVoiTTScl;
    @Column(name = "NLdavoiTT_SX")
    private Double nLDaVoiTTSx;
    @Column(name = "NLnuocTT")
    private Double nLNuocTT;
    @Column(name = "EDB_DH_DAU")
    private Double edbDhDau;
    @Column(name = "DN_PHANKHANG_NHAN")
    private Double dnPhankhangNhan;
    @Column(name = "DN_MBA_KICH_TU_TE")
    private Double dnMbaKichTuTe;
    @Column(name = "DN_TD_TD9")
    private Double dnTdTd9;
    @Column(name = "Nldaudo_NHIETTRI")
    private Double nlDauDoNhietTri;
    @Column(name = "Nldaufo_NHIETTRI")
    private Double nlDauFoNhietTri;


}
