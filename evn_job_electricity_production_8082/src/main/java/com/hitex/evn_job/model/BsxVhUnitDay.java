package com.hitex.evn_job.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.hitex.evn_job.utils.Constant;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
@Table(name = Constant.BSX_VH_UNIT_DAY, schema = Constant.GENERAL)
public class BsxVhUnitDay {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "unitid")
    private String unitId;
    @Column(name = "day")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime day;
    @Column(name = "EMF")
    private Double emf;
    @Column(name = "EMF_K")
    private Double emfK;
    @Column(name = "EMF_DO")
    private Double emfDo;
    @Column(name = "ETD")
    private Double etd;
    @Column(name = "ETD_K")
    private Double etdK;
    @Column(name = "ETD_DO")
    private Double etdDo;
    @Column(name = "ETDSXD")
    private Double etdsxd;
    @Column(name = "ETDXS")
    private Double etdxs;
    @Column(name = "ETDCB")
    private Double etdcb;
    @Column(name = "EDB")
    private Double edb;
    @Column(name = "EDB_K")
    private Double edbK;
    @Column(name = "EDB_DO")
    private Double edbDo;
    @Column(name = "OH")
    private Double oh;
    @Column(name = "OHLK")
    private Double ohlk;
    @Column(name = "EOH")
    private Double eoh;
    @Column(name = "EOHLK")
    private Double eohlk;
    @Column(name = "H_VHKHI")
    private Double hVhkhi;
    @Column(name = "H_VHDAU")
    private Double hVhdau;
    @Column(name = "H_DUNGDUPHONG")
    private Double hDungduphong;
    @Column(name = "H_DUNGSCL")
    private Double hDungscl;
    @Column(name = "H_DUNGSCTX")
    private Double hDungsctx;
    @Column(name = "H_DUNGSUCO")
    private Double hDungsuco;
    @Column(name = "H_DUNGSCBT")
    private Double hDungscbt;
    @Column(name = "H_DUNG_NGUYENNHAN")
    private String hDungNguyenNhan;
    @Column(name = "Qcm")
    private Double qcm;
    @Column(name = "KLNUOCCM")
    private Double klnuoccm;
    @Column(name = "USER_ID_DIEN")
    private String userIdDien;
    @Column(name = "USER_DTIME_DIEN")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime userDtimeDien;
    @Column(name = "USER_ID_H")
    private String userIdH;
    @Column(name = "USER_DTIME_H")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime userDtimeH;
    @Column(name = "USER_ID_TV")
    private String userIdTv;
    @Column(name = "USER_DTIME_TV")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime userDtimeTv;
    @Column(name = "DIEN_NANG_NHAN")
    private Double dienNangNhan;
    @Column(name = "DIEN_NANG_NHAN_CHAY_BU")
    private Double dienNangNhanChayBu;
    @Column(name = "DIEN_NANG_PHAN_KHANG")
    private Double dienNangPhanKhang;
    @Column(name = "TON_THAT_MBA_KICH_TU")
    private Double tonThatMbaKichTu;
    @Column(name = "TON_THAT_MBA_NANG")
    private Double tonThatMbaNang;
    @Column(name = "M_DUNGSCBT")
    private Double mDungscbt;
    @Column(name = "M_DUNGSCL")
    private Double mDungscl;
    @Column(name = "M_DUNGSUCO")
    private Double mDungsuco;
    @Column(name = "NLdaudoton_T")
    private Double nLDauDOTonT;
    @Column(name = "NLdaudott_KD")
    private Double nLDauDOTTKd;
    @Column(name = "NLdaudott_SCL")
    private Double nLDauDOTTScl;
    @Column(name = "NLdaudott_SX")
    private Double nLDauDOTTSx;
    @Column(name = "NLdaudott_T")
    private Double nLDauDOTTT;
    @Column(name = "NLDaufott_KD")
    private Double nLDauFOTTKd;
    @Column(name = "NLDaufott_SCL")
    private Double nLDauFOTTScl;
    @Column(name = "NLdaufott_SX")
    private Double nLdauFOTTSx;
    @Column(name = "NLdaufott_T")
    private Double nLDauFOTTT;
    @Column(name = "NLdaufoton_T")
    private Double nLDauFOTonT;
    @Column(name = "NLdavoiton_T")
    private Double nLDaVoiTonT;
    @Column(name = "NLdavoiTT_KD")
    private Double nLDaVoiTTKd;
    @Column(name = "NLdavoiTT_SCL")
    private Double nLDaVoiTTScl;
    @Column(name = "NLdavoiTT_SX")
    private Double nLDaVoiTTSx;
    @Column(name = "NLdavoiTT_T")
    private Double nLDaVoiTTT;
    @Column(name = "NLkhiTT_KD")
    private Double nLKhiTTKd;
    @Column(name = "NLkhiTT_SCL")
    private Double nLKhiTTScl;
    @Column(name = "NLkhiTT_SX")
    private Double nLKhiTTSx;
    @Column(name = "NLkhiTT_T")
    private Double nLkhiTTT;
    @Column(name = "NLnuocTT")
    private Double nLNuocTT;
    @Column(name = "NLthantonTC_T")
    private Double nLThanTonTCT;
    @Column(name = "NLthantonTN_T")
    private Double nLThanTonTNT;
    @Column(name = "NLthanTTTC_KD")
    private Double nLThanTTTCKd;
    @Column(name = "NLthanTTTC_SCL")
    private Double nLThanTTTCScl;
    @Column(name = "NLthanTTTC_SX")
    private Double nLThanTTTCSx;
    @Column(name = "NLthanTTTC_T")
    private Double nLThanTTTCT;
    @Column(name = "NLthanTTTN_KD")
    private Double nLThanTTTNKd;
    @Column(name = "NLthanTTTN_SCL")
    private Double nLThanTTTNScl;
    @Column(name = "NLthanTTTN_SX")
    private Double nLThanTTTNSx;
    @Column(name = "NLthanTTTN_T")
    private Double nLThanTTTNT;
    @Column(name = "U_P_DES")
    private Double uPDes;
    @Column(name = "U_P_LOST")
    private Double uPLost;
    @Column(name = "U_P_LOST_SCBT")
    private Double uPLostScbt;
    @Column(name = "U_P_LOST_SCL")
    private Double uPLostScl;
    @Column(name = "U_P_LOST_SUCO")
    private Double uPLostSuco;
    @Column(name = "khoi_dong")
    private int khoiDong;
    @Column(name = "H_CHAYBU")
    private Double hChayBu;
    @Column(name = "H_DIEUTAN")
    private Double hDieuTan;
    @Column(name = "DN_PHANKHANG_NHAN")
    private Double dnPhanKhangNhan;
    @Column(name = "DN_MBA_KICH_TU_TE")
    private Double dnMbaKichTuTe;
    @Column(name = "DN_TD_TD9")
    private Double dnTdTd9;
    @Column(name = "NLDauDO_NHIETTRI")
    private Double nlDauDoNhietTri;
    @Column(name = "NLDAUFO_NHIETTRI")
    private Double nlDauFoNhietTri;
}
