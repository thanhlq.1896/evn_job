package com.hitex.evn_job.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.hitex.evn_job.utils.Constant;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
@Table(name =  Constant.BSX_KH_PLANT_SX_NAME,schema = Constant.GENERAL)
public class BsxKhPlantSxNam {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "planid")
    private String planId;
    @Column(name = "nam")
    private int nam;
    @Column(name = "paramid")
    private String paramId;
    @Column(name = "t01")
    private Double t01;
    @Column(name = "t02")
    private Double t02;
    @Column(name = "t03")
    private Double t03;
    @Column(name = "t04")
    private Double t04;
    @Column(name = "t05")
    private Double t05;
    @Column(name = "t06")
    private Double t06;
    @Column(name = "t07")
    private Double t07;
    @Column(name = "t08")
    private Double t08;
    @Column(name = "t09")
    private Double t09;
    @Column(name = "t10")
    private Double t10;
    @Column(name = "t11")
    private Double t11;
    @Column(name = "t12")
    private Double t12;
    @Column(name = "cn")
    private Double cn;
    @Column(name = "user_cr_id")
    private String userCrId;
    @Column(name = "user_cr_dtime")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime userCrDtime;
    @Column(name = "user_mdf_id")
    private String userMdfId;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Column(name = "user_mdf_dtime")
    private LocalDateTime userMdfDtime;
}
