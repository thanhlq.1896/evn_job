package com.hitex.evn_job.service;


import com.hitex.evn_job.model.Notification;
import com.hitex.evn_job.repository.NotificationRepository;
import lombok.extern.log4j.Log4j2;
import org.aspectj.weaver.ast.Not;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
@Log4j2
public class NotificationService {

    @Autowired
    NotificationRepository notificationRepository;

    public List<Notification> getListNotificationForJobSchedule(int hour) {
        return notificationRepository.getListNotificationForJobSchedule(hour);
    }

    public List<Notification> getListNotificationForJob(Date date) {
        return notificationRepository.getListNotificationForJob(date);
    }

    public void saveListNotificationSendMail(List<Notification> notifications){
        try {
            notificationRepository.saveAll(notifications);
        }catch (Exception e){
            log.error("ERROR WHEN SAVE NOTIFICATION SEND EMAIL");
        }
    }

}
