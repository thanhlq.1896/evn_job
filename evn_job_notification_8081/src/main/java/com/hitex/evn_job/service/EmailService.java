package com.hitex.evn_job.service;

import com.hitex.evn_job.model.EmailConfig;
import com.hitex.evn_job.repository.EmailConfigRepository;
import com.hitex.evn_job.utils.Constant;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.mail.*;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.List;
import java.util.Properties;

@Service
@Log4j2
public class EmailService {
    @Autowired
    EmailConfigRepository emailConfigRepository;

    public void senMailMulti(String action, List<String> toMail, String content) {
        if (toMail.size() != 0) {
            new Thread(() -> {
                EmailConfig emailConfig = emailConfigRepository.findByAction(action);
                Properties properties = this.configEmailProperties();
                javax.mail.Session session = Session.getInstance(properties, new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(emailConfig.getEmail(), emailConfig.getPassword());
                    }
                });
                log.info("Send Email New Notification from email : {}, password : {}", emailConfig.getEmail(),emailConfig.getPassword());
                // compose message
                try {
                    MimeMessage message = new MimeMessage(session);
                    message.setRecipients(Message.RecipientType.TO, getAddress(toMail));
                    message.setSubject(emailConfig.getSubject());
                    message.setText(content);

                    // send message
                    Transport.send(message);
                } catch (MessagingException e) {
                    throw new RuntimeException(e);
                }
            }).start();
        }
    }

    public InternetAddress[] getAddress(List<String> mailAddress) throws AddressException {
        log.info("list email receive email is :");
        InternetAddress[] addresses = new InternetAddress[mailAddress.size()];
        for (int i = 0; i < mailAddress.size(); i++) {
            log.info(mailAddress.get(i));
            InternetAddress internetAddress = new InternetAddress(mailAddress.get(i).trim());
            addresses[i] = internetAddress;
        }
        return addresses;
    }

    private Properties configEmailProperties() {
        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", Constant.HOST_NAME);
        props.put("mail.smtp.port", Constant.TSL_PORT);
        return props;
    }

}
