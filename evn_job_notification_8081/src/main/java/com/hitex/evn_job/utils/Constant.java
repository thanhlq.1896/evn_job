package com.hitex.evn_job.utils;

import java.time.format.DateTimeFormatter;

public class Constant {
    public static final String EMAIL_CONFIG = "email_config";
    // Config mail
    public static final String HOST_NAME = "smtp.gmail.com";
    public static final int TSL_PORT = 587; // Port for TLS/STARTTLS
    public static final String GENERAL = "general";
    public static final String ACTION_MAIL_SEND_NOTIFICATION = "SEND_NOTIFICATION";

}
