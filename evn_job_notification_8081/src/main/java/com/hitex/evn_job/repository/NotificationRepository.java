package com.hitex.evn_job.repository;

import com.hitex.evn_job.model.Notification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;
import java.util.List;

public interface NotificationRepository extends JpaRepository<Notification, Integer> {

    @Query("select n from Notification n where n.typeSchedule not in(0,1,2) and n.hour = ?1")
    List<Notification> getListNotificationForJobSchedule(int hour);

    @Query("select n from Notification n where n.typeSchedule = 2 and n.timeSendEmail <= ?1 and n.isSendMail = false")
    List<Notification> getListNotificationForJob(Date currentDate);
}
