package com.hitex.evn_job.repository;

import com.hitex.evn_job.model.EmailConfig;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface EmailConfigRepository extends JpaRepository<EmailConfig, Integer> {
    @Query(value = "select e from EmailConfig e where e.action = ?1")
    EmailConfig findByAction(String action);
}
