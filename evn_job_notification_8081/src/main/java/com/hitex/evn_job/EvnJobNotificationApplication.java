package com.hitex.evn_job;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class EvnJobNotificationApplication {

    public static void main(String[] args) {
        SpringApplication.run(EvnJobNotificationApplication.class, args);
    }

}
