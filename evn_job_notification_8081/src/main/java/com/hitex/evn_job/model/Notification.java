package com.hitex.evn_job.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;

@Data
@Entity
@Table(name = "notification")
public class Notification {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "title_notification")
    private String titleNotification;

    @Column(name = "short_description")
    private String shortDescription;

    @Column(name = "content")
    private String content;

    @Column(name = "created")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Timestamp created;

    @Column(name = "updated")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Timestamp updated;

    @Column(name = "status")
    private int status;

    @Column(name = "is_public")
    private int isPublic;

    @Column(name = "id_category")
    private int idCategory;

    @Column(name = "list_mail")
    private String listMail;

    /*
     * 0 : Không gửi, 1 : Gửi ngay, 2: Gửi giờ cố định
     * 3 : Hàng ngày, 4 : Hàng tuần
     * 5 : Hàng tháng, 6 : Hàng năm
     */
    //Need to add for cronJob
    @Column(name = "type_schedule")
    private Integer typeSchedule;
    @Column(name = "time_send_email")
    private Date timeSendEmail;
    @Column(name = "hour")
    private Integer hour;
    @Column(name = "day_of_week")
    private Integer dayOfWeek;
    @Column(name = "day_of_month")
    private Integer dayOfMonth;
    @Column(name = "month")
    private Integer month;
    @Column(name = "is_send_mail")
    private boolean isSendMail;


}
