package com.hitex.evn_job.job;

import com.hitex.evn_job.model.Notification;
import com.hitex.evn_job.service.EmailService;
import com.hitex.evn_job.service.NotificationService;
import com.hitex.evn_job.utils.Constant;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * @author Thanh
 */

@Component
@Log4j2
public class NotificationJob {

    @Autowired
    NotificationService notificationService;

    @Autowired
    EmailService emailService;

    /**
     * This job will run each minute,
     * Get notifications have type = 2 (has fixed dateTime)
     * and have timeSendMail <= currentDate
     * and isSendMail = false (Haven't send before)
     * If send successfully, then saved notification.isSendMail = true;
     *
     * @author Thanhlq
     */
    @Scheduled(cron = "${job.send-email-notification-schedule-one-minute}")
    public void sendNotificationJobOnce() {
        System.out.println("JOB SEND MAIL FOR A FIXED DATETIME");
        List<Notification> listNotificationForJob = notificationService.getListNotificationForJob(new Date());

        for (Notification notification : listNotificationForJob) {
            List<String> listEmail;
            if (notification.getListMail() != null && !notification.getListMail().equals("")) {
                listEmail = Arrays.asList(notification.getListMail().split(","));
                emailService.senMailMulti(Constant.ACTION_MAIL_SEND_NOTIFICATION, listEmail, notification.getContent());
                notification.setSendMail(true);
            }
        }
        notificationService.saveListNotificationSendMail(listNotificationForJob);
    }


    /**
     * This job will run each hour,
     * Get notifications have type = 3,4,5,6 (has schedule time)
     *
     * @author Thanhlq
     */
    @Scheduled(cron = "${job.send-email-notification-schedule-one-hour}")
    public void sendNotificationJob_ScheduleOneHour() {
        System.out.println("JOB SEND MAIL FOR SCHEDULE TIME : ");
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
        int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
        int month = calendar.get(Calendar.MONTH);

        List<Notification> result = new LinkedList<>();
        List<Notification> listNotificationForJob = notificationService.getListNotificationForJobSchedule(hour);
        for (Notification notification : listNotificationForJob) {
            if (checkCanSendEmail(notification, hour, dayOfWeek, dayOfMonth, month)) {
                result.add(notification);
            }
        }

        for (Notification notification : result) {
            List<String> listEmail;
            if (notification.getListMail() != null && !notification.getListMail().equals("")) {
                listEmail = Arrays.asList(notification.getListMail().split(","));
                emailService.senMailMulti(Constant.ACTION_MAIL_SEND_NOTIFICATION, listEmail, notification.getContent());
            }
        }
    }

    private boolean checkCanSendEmail(Notification notification, int hour, int dayOfWeek, int dayOfMonth, int month) {
        boolean isTypeSchedule_Daily = notification.getTypeSchedule() == 3 &&
                notification.getHour() == hour;
        boolean isTypeSchedule_Weekly = notification.getTypeSchedule() == 4 &&
                notification.getHour() == hour &&
                notification.getDayOfWeek() == dayOfWeek;
        boolean isTypeSchedule_Monthly = notification.getTypeSchedule() == 5 &&
                notification.getHour() == hour &&
                notification.getDayOfMonth() == dayOfMonth;
        boolean isTypeSchedule_Yearly = notification.getTypeSchedule() == 6 &&
                notification.getHour() == hour &&
                notification.getDayOfMonth() == dayOfMonth &&
                notification.getMonth() == month;
        return isTypeSchedule_Daily || isTypeSchedule_Weekly || isTypeSchedule_Monthly || isTypeSchedule_Yearly;
    }
}
